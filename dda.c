#include <stdio.h>

int img[500][500];

typedef struct xy
{
    int x;
    int y;
} xy;

typedef struct line
{
    xy from;
    xy to;
} line;

void init(int color);
int roundup(float num);
void dda(line l);
void createPGM();

int main(int argc, char const *argv[])
{
    line l;
    l.from.x = 0;
    l.from.y = 0;
    l.to.x = 50;
    l.to.y = 100;
    
    init(0);
    dda(l);
    createPGM();
    
    return 0;
}


void dda(line l)
{
    int dx = l.to.x - l.from.x;
    int dy = l.to.y - l.from.y;

    float x_change, y_change;
    int steps, i;
    float m;
    float x = l.from.x;
    float y = l.from.y;

    m = (float)abs(dy) / (float)abs(dx);

    if (dx != 0 && dy != 0)
    {
        if (m > (float)1)
        {
            y_change = dy > 0 ? 1 : -1;
            x_change = dx > 0 ? 1 / m : -1 / m;
        }
        else if (m == (float)1)
        {
            y_change = dy > 0 ? 1 : -1;
            x_change = dx > 0 ? 1 : -1;
        }
        else
        {
            y_change = dy > 0 ? m : -m;
            x_change = dx > 0 ? 1 : -1;
        }
    }
    else if (dy == 0)
    {
        y_change = 0;
        x_change = dx > 0 ? 1 : -1;
    }
    else if (dx == 0)
    {
        y_change = dy > 0 ? 1 : -1;
        x_change = 0;
    }

    steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    for( i = 0; i <= steps ; i++)
    {   
        img[roundup(x)][roundup(y)] = 255 ;
        x = x + x_change;
        y = y + y_change;

    }

}

int roundup(float num)
{
    return (int)(num+0.5);
}

void createPGM()
{
    FILE *image;
    image = fopen("dda.pgm","wb");
    fprintf(image,"P2\n");
    fprintf(image,"500 500\n");
    fprintf(image,"255\n");

    for( int i = 0 ; i <500; i++)
    {
        for( int j = 0 ; j <500 ; j++)
        {
            fprintf(image, "%d ",img[i][j]);
        }

    }
    fclose(image);
}

void init(int color)
{
    for( int i = 0 ; i < 500 ;i++)
    {
        for ( int j = 0 ; j <500  ; j++)
        {
            img[i][j]=color;
        }
    }
}