#include <stdio.h>

int img[500][500];

typedef struct xy
{
    int x;
    int y;
} xy;

typedef struct circle
{
    xy centre;
    int radius;
} circle;

void init(int color);
int roundup(float num);
void bresenhamcircle(circle c);
void plotPointInOctants(circle c, int x, int y);
void createPGM();

int main(int argc, char const *argv[])
{
    circle c;
    c.centre.x = 250;
    c.centre.y = 250;
    c.radius = 100;

    init(0);
    bresenhamcircle(c);
    createPGM();

    return 0;
}

void bresenhamcircle(circle c)
{
    int x = 0;
    int y = c.radius;
    plotPointInOctants(c, x, y);
    int pk = 3 - 2 * c.radius;
    while (x < y)
    {
        x++;
        if (pk < 0)
        {
            pk += 4 * x + 6;
        }
        else
        {
            y--;
            pk += 4 * x - 4 * y + 10;
        }

        plotPointInOctants(c, x, y);
    }
}

void plotPointInOctants(circle c, int x, int y)
{
    img[c.centre.x + x][c.centre.y + y] = 255;
    img[c.centre.x + x][c.centre.y - y] = 255;
    img[c.centre.x - x][c.centre.y + y] = 255;
    img[c.centre.x - x][c.centre.y - y] = 255;
    img[c.centre.x + y][c.centre.y + x] = 255;
    img[c.centre.x + y][c.centre.y - x] = 255;
    img[c.centre.x - y][c.centre.y + x] = 255;
    img[c.centre.x - y][c.centre.y - x] = 255;
}

int roundup(float num)
{
    return (int)(num + 0.5);
}

void createPGM()
{
    FILE *image;
    image = fopen("bresenhamcircle.pgm", "wb");
    fprintf(image, "P2\n");
    fprintf(image, "500 500\n");
    fprintf(image, "255\n");

    for (int i = 0; i < 500; i++)
    {
        for (int j = 0; j < 500; j++)
        {
            fprintf(image, "%d ", img[j][i]);
        }
    }
    fclose(image);
}

void init(int color)
{
    for (int i = 0; i < 500; i++)
    {
        for (int j = 0; j < 500; j++)
        {
            img[i][j] = color;
        }
    }
}