#include <stdio.h>

int img[500][500];

typedef struct xy
{
    int x;
    int y;
} xy;

typedef struct line
{
    xy from;
    xy to;
} line;

void init(int color);
int roundup(float num);
void midpointline(line l);
void createPGM();

int main(int argc, char const *argv[])
{
    line l;
    l.from.x = 0;
    l.from.y = 0;
    l.to.x = 300;
    l.to.y = 100;
    
    init(0);
    midpointline(l);
    createPGM();
    
    return 0;
}


void midpointline(line l)
{
    int dx = l.to.x - l.from.x;
    int dy = l.to.y - l.from.y;

    int x_change, y_change;
    int steps, i;
    float m;
    int x = l.from.x;
    int y = l.from.y;

    m = (float)abs(dy) / (float)abs(dx);

    if (dx != 0 && dy != 0)
    {
        if (m > (float)1)
        {
            y_change = dy > 0 ? 1 : -1;
        }
        else if (m == (float)1)
        {
            y_change = dy > 0 ? 1 : -1;
            x_change = dx > 0 ? 1 : -1;
        }
        else
        {
            x_change = dx > 0 ? 1 : -1;
        }
    }
    else if (dy == 0)
    {
        y_change = 0;
        x_change = dx > 0 ? 1 : -1;
    }
    else if (dx == 0)
    {
        y_change = dy > 0 ? 1 : -1;
        x_change = 0;
    }

    steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    int pk;

    if(dx!=0 && dy!=0)
    {   pk = -abs(dx)*(y+1)+abs(dy)*(x+0.5)+abs(dx)*(y-m*x);
        if(m>=(float)1)
        {   
            while(y!=l.to.y)
            {   
                img[x][y]=255;
                
                if(pk>=0)
                {
                    x_change = dx>0?1:-1;
                    pk+=abs(dx)-abs(dy);
                }
                else
                {   
                    x_change =0;
                    pk+=abs(dx);
                }

                y+=y_change;
                x+=x_change;
            }
            
        }
        else
        {   pk = -abs(dx)*(y+0.5)+abs(dy)*(x+1)+abs(dx)*(y-m*x);
            while(x!=l.to.x)
            {   
                img[x][y]=255;
                if(pk>=0)
                {
                    y_change = dy>0?1:-1;
                    pk+=abs(dy)-abs(dx);
                }
                else
                {   
                    y_change =0;
                    pk+=abs(dy);
                }
                y+=y_change;
                x+=x_change;
            }
            
        }
    }
    else
    {
        for( i =0;i<=steps ;i++){
            img[x][y]=255;
            x+=x_change;
            y+=y_change;
        }
    }

}

int roundup(float num)
{
    return (int)(num+0.5);
}

void createPGM()
{
    FILE *image;
    image = fopen("midpointline.pgm","wb");
    fprintf(image,"P2\n");
    fprintf(image,"500 500\n");
    fprintf(image,"255\n");

    for( int i = 0 ; i <500; i++)
    {
        for( int j = 0 ; j <500 ; j++)
        {
            fprintf(image, "%d ",img[j][i]);
        }

    }
    fclose(image);
}

void init(int color)
{
    for( int i = 0 ; i < 500 ;i++)
    {
        for ( int j = 0 ; j <500  ; j++)
        {
            img[i][j]=color;
        }
    }
}