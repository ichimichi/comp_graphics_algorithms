#include <stdio.h>

int img[500][500];

typedef struct xy
{
    int x;
    int y;
} xy;

typedef struct ellipse
{
    int Rx;
    int Ry;
    xy center;
} ellipse;

void init(int color);
int roundup(float num);
void midpointellipse(ellipse e);
void plotPointInQuadrants(ellipse e, int x, int y);
void createPGM();

int main(int argc, char const *argv[])
{
    ellipse e;
    e.center.x = 250;
    e.center.y = 250;
    e.Rx = 100;
    e.Ry = 50;

    init(0);
    midpointellipse(e);
    createPGM();

    return 0;
}

void midpointellipse(ellipse e)
{
    int x = 0;
    int y = e.Ry;
    int pk;
    int Rx_sq = e.Rx * e.Rx;
    int Ry_sq = e.Ry * e.Ry;
    int two_Rx_sq = 2 * Rx_sq;
    int two_Ry_sq = 2 * Ry_sq;

    int px = 0;
    int py = two_Rx_sq * e.Ry ;

    plotPointInQuadrants(e, x, y);

    pk = ((int)((Ry_sq + 0.25 * Rx_sq - Rx_sq * e.Ry) + 0.5));

    while ( px < py)
    {
        x++;
        px+=two_Ry_sq;

        if(pk>=0)
        {
            y--;
            py-=two_Rx_sq;
            pk+=px+Ry_sq-py;
        }
        else
        {
            pk+=px+Ry_sq;
        }
        plotPointInQuadrants(e,x,y);
    }

    pk = ((int)((Ry_sq * (x + 0.5) * (x + 0.5) + Rx_sq * (y - 1) * (y - 1) - Rx_sq * Ry_sq) + 0.5));

    while (y > 0)
    {
        y--;
        py -= two_Rx_sq;
        if (pk > 0)
        {
            pk += Rx_sq - py;
        }
        else
        {
            x++;
            px += two_Ry_sq;
            pk += Rx_sq - py + px;
        }

        plotPointInQuadrants(e, x, y);
    }
    
}

void plotPointInQuadrants(ellipse e, int x, int y)
{
    img[e.center.x + x][e.center.y + y] = 255;
    img[e.center.x + x][e.center.y - y] = 255;
    img[e.center.x - x][e.center.y + y] = 255;
    img[e.center.x - x][e.center.y - y] = 255;
    // img[e.center.x + y][e.center.y + x] = 255;
    // img[e.center.x + y][e.center.y - x] = 255;
    // img[e.center.x - y][e.center.y + x] = 255;
    // img[e.center.x - y][e.center.y - x] = 255;
}

int roundup(float num)
{
    return (int)(num + 0.5);
}

void createPGM()
{
    FILE *image;
    image = fopen("midpointellipse.pgm", "wb");
    fprintf(image, "P2\n");
    fprintf(image, "500 500\n");
    fprintf(image, "255\n");

    for (int i = 0; i < 500; i++)
    {
        for (int j = 0; j < 500; j++)
        {
            fprintf(image, "%d ", img[j][i]);
        }
    }
    fclose(image);
}

void init(int color)
{
    for (int i = 0; i < 500; i++)
    {
        for (int j = 0; j < 500; j++)
        {
            img[i][j] = color;
        }
    }
}